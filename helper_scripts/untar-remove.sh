#!/bin/bash

function _init ()
{
    set -e;

    if [ $# -lt 2 ]; then
	echo "Continuing tests with current working directory."
        M=$PWD;
    else
	if [ ! -d $2 ]; then
	    echo "2nd argument needs to be a valid directory."
	    exit 42;
	else
            M=$2;
	fi
    fi

    if echo $1 | egrep -q '^[0-9]+$'; then
	T=$1;
    else
	echo "First argument was not a number, continuing with default"
	T=600;
    fi
    echo $T
    export pid_untar;
    export pid_remove;
}

function run_tests()
{
    old_PWD=$PWD;
    cd $M;

    sleep 1;
    untar &
    sleep 2;
    remove_kernel &

    sleep $T;

    cd $old_PWD;
}

function untar ()
{
    cd $M;
    while true
    do
       tar -xjf /opt/qa/tools/linux-2.6.31.1.tar.bz2 &
       pid_untar=$!;
       wait $pid_untar;
    done
}

function remove_kernel ()
{
    while true
    do
        rm -rf $M/linux-2.6.31.1 &
	pid_remove=$!;
	wait $pid_remove;
    done
}

function watchdog ()
{
    # insurance against hangs during the test

    sleep $1;

    echo "Kicking in watchdog after $1 secs";
}


function finish ()
{
    for i in $(jobs -pr)
    do
	echo "killing jobs"
    	kill -KILL $i;
	sleep 2;
    done

    sleep 10;

    rm -rf $M/linux-2.6.31.1 &
    cleanup_remove=$!;
    wait $cleanup_remove;
}

function main ()
{
    watchdog $T &

    trap finish INT EXIT;

    set -x;

    run_tests;
}

_init "$@" && main "$@";
